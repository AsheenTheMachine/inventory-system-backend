﻿
using App.Domain.Helpers;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace App.Domain
{
    public class Product : BaseEntity
    {
        public string Code { get; set; }

        //concurrency check to make sure 2 updates don't conflict
        [ConcurrencyCheck]        
        public string Name { get; set; }
        
        public string Description { get; set; }
        
        public long CategoryId { get; set; }

        [Column(TypeName = "money")]
        public decimal Price { get; set; }

        public byte[] Image { get; set; }

    }
}
