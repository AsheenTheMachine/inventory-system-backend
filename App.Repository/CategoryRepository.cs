﻿using System.Linq;
using App.Domain;
using App.Repository.Helpers;
using App.Domain.Helpers;
using App.Repository.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace App.Repository
{
    public class CategoryRepository : GenericRepository<Category>, ICategoryRepository
    {
        public CategoryRepository(DataContext context) : base(context) { }

        //fetch list of categories by userId
        public async Task<List<Category>> GetAllByUserId(long userId)
        {
            return await _context.Category
                            .Where(c => c.UserId == userId)
                            .OrderBy(x => x.Name)
                            .ToListAsync();
        }

        //fetch by category code
        public async Task<Category> GetByCode(string code)
        {
            return await _context.Category
                            .Where(c => c.Code.Trim() == code)
                            .FirstOrDefaultAsync();
        }
    }
}
