﻿using App.Model.Helpers;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace App.Model
{
    public class CategoryModel : BaseModel
    {
        [Required(ErrorMessage = "Category Code is required!")]
        [StringLength(6, ErrorMessage = "Category Code must contain 6 characters!", MinimumLength = 6)]
        public string Code { get; set; }
        
        [Required(ErrorMessage = "Category Name is required!")]
        [StringLength(50, ErrorMessage = "Category Name cannot be longer than 200 charaters!")]
        public string Name { get; set; }
        
        [Required]
        public bool IsActive { get; set; }

        [Required]
        public long UserId { get; set; }
    }
}
