﻿using App.Model.Helpers;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace App.Model
{
    public class ProductModel : BaseModel
    {
        [Required(ErrorMessage = "Product Code is required!")]
        [StringLength(15, ErrorMessage = "Product Code exceeded the format limit of 15 characters!")]
        public string Code { get; set; }
        
        [Required(ErrorMessage = "Product Name is required!")]
        [StringLength(200, ErrorMessage = "Product Name cannot be longer than 200 characters!")]
        public string Name { get; set; }
        
        public string Description { get; set; }

        [Required]
        public long CategoryId { get; set; }

        [Required(ErrorMessage = "Price is required!")]
        public decimal Price { get; set; }

        public byte[] Image { get; set; }

    }
}
