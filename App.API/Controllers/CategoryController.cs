﻿using System;
using System.Threading.Tasks;
using App.Api.Helpers;
using App.Model;
using App.Repository.Helpers;
using App.Repository.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using App.Domain;
using AutoMapper;
using System.Net;
using App.Api.Services;
using Microsoft.AspNetCore.Authorization;

namespace App.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : BaseController
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ICategoryRepository _categoryRepository;

        public CategoryController(IUnitOfWork unitOfWork,
            IMapper mapper, 
            ICategoryRepository categoryRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;

            _categoryRepository = categoryRepository;
        }

 
        [HttpGet]
         public IActionResult Index()
        {
            return Ok(HttpStatusCode.NotImplemented);
        }

        /// <summary>
        /// Adds a new category
        /// </summary>
        /// <param name="CategoryModel"></param>
        /// <returns></returns>
        [HttpPost("new")]
        public async Task<IActionResult> CreateCategory([FromBody] CategoryModel model)
        {
            #region Validation
            if (!ModelState.IsValid)
                return BadRequest(new { message = GetErrors() });
            // if(BusinessRulesService.IsCategoryCodeValid(model.Code))
            //     return BadRequest(new { message = "Category Code is invalid. Must contain 3 letters and 3 numbers!" });
            
            //check if category code already exists
            var catExists = await _unitOfWork.Category.GetByCode(model.Code);
            await _unitOfWork.Complete();

            if(catExists != null)
                return BadRequest(new { message = "Category Code already exists!" });

            #endregion

            var category = _mapper.Map<Category>(model);

            try
            {
                // add category object for inserting
                await _unitOfWork.Category.Add(category);
                int complete = await _unitOfWork.Complete();

                return Ok(complete);
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }
        
        /// <summary>
        /// Fetch category for edit, by id
        /// </summary>
        /// <param name="id">id of category object</param>
        /// <param name="id">userId the category belongs too</param>
        /// <returns>CategoryModel</returns>
        [HttpGet("{id}/{userId}")]
        public async Task<IActionResult> GetCategoryById(long id, long userId)
        {
            #region Validation

            if (id <= 0)
                return BadRequest(new { message = "Id must be greater than 0!" });

            #endregion

            try
            {
                //fetch category by id
                var category = await _unitOfWork.Category.Get(id);
                await _unitOfWork.Complete();

                var categoryModel = _mapper.Map<CategoryModel>(category);

                if(category == null)
                    return BadRequest(new { message = "Category does not exist!" });
                else
                {
                    //check if this category belongs to the user
                    if(categoryModel.UserId != userId)
                        return BadRequest(new { message = "You are not authorized to view this category!" });
                        //TODO: send security notification email to relvant people regarding possible breach
                }

                return Ok(categoryModel);
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }


        /// <summary>
        /// Edit a category
        /// </summary>
        /// <param name="id">id of the object to update</param>
        /// <param name="CategoryModel">model of data to update</param>
        /// <returns></returns>
        [HttpPut("edit/{id}")]
        public async Task<IActionResult> UpdateCategory([FromBody] CategoryModel model, long id)
        {
            #region Validation
            if (!ModelState.IsValid)
                return BadRequest(new { message = GetErrors() });
            if (id <= 0)
                return BadRequest(new { message = "Id must be greater than 0" });
            if(BusinessRulesService.IsCategoryCodeValid(model.Code))
                return BadRequest(new { message = "Category Code is invalid. Must contain 3 letters and 3 numbers!" });
            
            #endregion

            try
            {
                Category category = await _unitOfWork.Category.Get(id);
                await _unitOfWork.Complete();
                category.Code = model.Code;
                category.Name = model.Name;
                category.IsActive = model.IsActive;

                // add category object for updating
                _unitOfWork.Category.Update(category);
                return Ok(await _unitOfWork.Complete());
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }


        /// <summary>
        /// Delete a category
        /// </summary>
        /// <param name="CategoryModel"></param>
        /// <returns></returns>
        [HttpDelete("remove/{id}")]
        public async Task<IActionResult> RemoveCategory(long id)
        {
            #region Validation

            if (id <= 0)
                return BadRequest(new { message = "Id must be greater than 0" });

            #endregion

            try
            {
                var category = await _unitOfWork.Category.Get(id);
                _unitOfWork.Category.Delete(category);

                return Ok(await _unitOfWork.Complete());
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }
    
        /// <summary>
        /// Fetch category List by UserId
        /// </summary>
        /// <returns>A list of categories by user id</returns>
        [HttpGet("list/{userId}")]
        public async Task<IActionResult> GetAllByUserId(long userId)
        {
            #region Validation
            if (userId <= 0)
                return BadRequest(new { message = "userId must be greater than 0" });
            #endregion

            try
            {
                var categoryList =  await _unitOfWork.Category.GetAllByUserId(userId);
                await _unitOfWork.Complete();
                return Ok(categoryList);
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }
    }
}
