﻿using Microsoft.AspNetCore.Mvc;
using App.Repository.Helpers;
using System;
using App.Model;
using AutoMapper;
using App.Api.Helpers;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using App.Domain;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;
using System.Net;
using App.Api.Services;
using System.Net.Mail;

namespace App.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : BaseController
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly AppSettings _appSettings;

        private readonly IEmailService _emailService;

        public UserController(IUnitOfWork unitOfWork,
            IMapper mapper,
            IOptions<AppSettings> appSettings,
            IEmailService emailService)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _appSettings = appSettings.Value;
            _emailService = emailService;
        }


        //authenticate
        [HttpGet]
        public IActionResult Index()
        {
            return Ok(HttpStatusCode.NotImplemented);
        }

        //authenticate
        [HttpPost("authenticate")]
        public async Task<IActionResult> Authenticate([FromBody] AuthenticateModel model)
        {
            #region Validation

            if (!ModelState.IsValid)
                return BadRequest(new { message = GetErrors() });

            var user = await _unitOfWork.User.GetByEmail(model.Email);
            await _unitOfWork.Complete();

            // check if user exists
            if (user == null)
                return BadRequest(new { message = "Email is incorrect!" });

            if (!user.IsActive ||
                (
                user.VerifyToken.HasValue &&
                user.VerifyToken.Value != Guid.Empty
                )
            ) return BadRequest(new { message = "Account not verified!" });

            // check if password is correct
            if (!UserSecurity.VerifyPasswordHash(model.Password, user.PasswordHash, user.PasswordSalt))
                return BadRequest(new { message = "Password is incorrect!" });

            #endregion

            //create JWT
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);

            // return basic user info and authentication token
            return Ok(new
            {
                user.Id,
                user.Email,
                user.Name,
                user.Surname,
                Token = tokenString
            });
        }

        //register
        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] RegisterModel model)
        {
            // map model to entity
            var user = _mapper.Map<User>(model);

            //check if email already exists
            var existingUser = await _unitOfWork.User.GetByEmail(model.Email);
            await _unitOfWork.Complete();

            // check if user exists
            if (existingUser != null)
                return BadRequest(new { message = "Email already exists!" });

            try
            {
                byte[] passwordHash, passwordSalt;
                UserSecurity.CreatePasswordHash(model.Password, out passwordHash, out passwordSalt);

                user.PasswordHash = passwordHash;
                user.PasswordSalt = passwordSalt;

                // create user
                await _unitOfWork.User.Add(user);
                await _unitOfWork.Complete();

                //send verification email
                await SendVerificationEmail(user);

                return Ok(new { message = "Account registered!" });
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
            catch (SmtpException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = "Verification email not sent." + ex.Message });
            }
        }

        private async Task SendVerificationEmail(User user)
        {
            string message;
            string origin = Request.Headers["origin"];

            var verifyUrl = $"{origin}/account/verify-email?token={user.VerifyToken}";
            message = $@"<p>Please click the below link to verify your email address:</p>
                             <p><a href=""{verifyUrl}"">{verifyUrl}</a></p>";

            await _emailService.Send(
                to: user.Email,
                subject: "Inventory System - Account Verification",
                html: $@"<h4>Verify Email</h4>
                         <p>Thanks for registering!</p>
                         {message}",
                from: _appSettings.EmailFrom
            );
        }

        //authenticate
        [HttpGet("verify-email")]
        public async Task<IActionResult> VerifyEmail([FromQuery] string token)
        {
            #region Validation
            if (string.IsNullOrEmpty(token))
                return BadRequest(new { message = "Token required!" });

            Guid _token;
            bool isValid = Guid.TryParse(token, out _token);

            if (!isValid)
                return BadRequest(new { message = "Invalid token submitted!" });
            #endregion

            var user = await _unitOfWork.User.GetByToken(_token);
            await _unitOfWork.Complete();

            if (user == null)
                return BadRequest(new { message = "Account not found!" });
            else
            {
                user.IsActive = true;
                user.VerifyToken = Guid.Empty;

                _unitOfWork.User.Update(user);
                await _unitOfWork.Complete();

                return Ok(new
                {
                    message = "success"
                });
            }
        }

    }
}
