using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using App.Api.Helpers;

namespace App.Api.Services
{
    public static class BusinessRulesService
    {
        // check if category code contains 3 letters and 3 numbers
        public static bool IsCategoryCodeValid(string code)
        {
            Regex regexObj = new Regex(@"([A-Za-z]{3}[0-9]{3})", RegexOptions.Singleline);
            return regexObj.IsMatch(code.Trim());
        }

        //create unique product code
        //format Year Month sequential code ‘yyyyMM-###’ e.g., 202111-023
        public static string GenerateProductCode()
        {
            Random rng = new Random();
            int value = rng.Next(1000);

            return string.Format("{0}{1}-{2}", 
            DateTime.Now.Year,
            DateTime.Now.Month,
            value.ToString("000"));
        }
    }
}