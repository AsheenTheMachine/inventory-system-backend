using System.Threading.Tasks;
using App.Api.Helpers;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Options;
using MimeKit;
using MimeKit.Text;

namespace App.Api.Services
{
    public class EmailService : IEmailService
    {
        private readonly AppSettings _appSettings;
        public EmailService(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }
        public async Task Send(string to, string subject, string html, string from = null)
        {
            // create message
            var email = new MimeMessage();
            email.From.Add(MailboxAddress.Parse(from ?? _appSettings.EmailFrom));
            email.To.Add(MailboxAddress.Parse(to));
            email.Subject = subject;
            email.Body = new TextPart(TextFormat.Html) { Text = html };

            using (var client = new SmtpClient())
            {
                await client.ConnectAsync(_appSettings.SmtpHost,
                                            _appSettings.SmtpPort,
                                            SecureSocketOptions.StartTls);

                // Since we don't have an OAuth2 token, disable
                // the XOAUTH2 authentication mechanism.
                client.AuthenticationMechanisms.Remove("XOAUTH2");

                await client.AuthenticateAsync(_appSettings.SmtpUsername, _appSettings.SmtpPassword);
                await client.SendAsync(email);
                await client.DisconnectAsync(true);
            }
        }
    }
}