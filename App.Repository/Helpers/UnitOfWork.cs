﻿using System;
using App.Repository.Interfaces;
using App.Domain.Helpers;
using System.Threading.Tasks;

namespace App.Repository.Helpers
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DataContext _context;

        public IUserRepository User { get; }
        public ICategoryRepository Category { get; }
        public IProductRepository Product { get; }

        public UnitOfWork(DataContext dataContext, 
            IUserRepository userRepository,
            ICategoryRepository categoryRepository,
            IProductRepository productRepository)
        {
            this._context = dataContext;
            this.User = userRepository;
            this.Category = categoryRepository;
            this.Product = productRepository;
        }

        public async Task<int> Complete()
        {
            return await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
        }
    }
}
