﻿using System;
using System.Threading.Tasks;
using App.Api.Helpers;
using App.Model;
using App.Repository.Helpers;
using App.Repository.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using App.Domain;
using AutoMapper;
using System.Net;
using System.IO;
using OfficeOpenXml;
using System.Collections.Generic;
using System.Linq;
using OfficeOpenXml.Style;
using System.Drawing;
using System.Globalization;
using App.Api.Services;
using Microsoft.AspNetCore.Authorization;

namespace App.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : BaseController
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IProductRepository _productRepository;

        public ProductController(IUnitOfWork unitOfWork,
            IMapper mapper,
            IProductRepository productRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;

            _productRepository = productRepository;
        }


        [HttpGet]
        public HttpStatusCode Index()
        {
            return HttpStatusCode.NotImplemented;
        }

        /// <summary>
        /// Adds a new product
        /// </summary>
        /// <param name="ProductModel"></param>
        /// <returns></returns>
        [HttpPost("new")]
        public async Task<IActionResult> CreateProduct([FromBody] ProductModel model)
        {
            #region Validation
            if (!ModelState.IsValid)
                return BadRequest(new { message = GetErrors() });
            #endregion

            var product = _mapper.Map<Product>(model);

            try
            {
                //create unique product code
                product.Code = BusinessRulesService.GenerateProductCode();

                // add product object for inserting
                await _unitOfWork.Product.Add(product);
                return Ok(await _unitOfWork.Complete());
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }

        /// <summary>
        /// Fetch product for edit
        /// </summary>
        /// <param name="id">id of product object</param>
        /// <returns>ProductModel</returns>
        [HttpGet("edit/{id}")]
        public async Task<IActionResult> GetProduct(long id)
        {
            #region Validation
            if (id <= 0)
                return BadRequest(new { message = "Id must be greater than 0" });
            #endregion

            try
            {
                //fetch product
                var product = await _unitOfWork.Product.Get(id);
                await _unitOfWork.Complete();

                var productModel = _mapper.Map<ProductModel>(product);

                return Ok(productModel);
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }


        /// <summary>
        /// Edit a product
        /// </summary>
        /// <param name="id">id of the object to update</param>
        /// <param name="ProductModel">model of data to update</param>
        /// <returns></returns>
        [HttpPut("edit/{id}")]
        public async Task<IActionResult> UpdateProduct([FromBody] ProductModel model, long id)
        {
            #region Validation
            if (!ModelState.IsValid)
                return BadRequest(new { message = GetErrors() });
            if (id <= 0)
                return BadRequest(new { message = "Id must be greater than 0" });
            #endregion

            try
            {
                var product = _mapper.Map<Product>(model);
                product.Id = id;

                // add product object for updating
                _unitOfWork.Product.Update(product);
                return Ok(await _unitOfWork.Complete());
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }


        /// <summary>
        /// Delete a Product
        /// </summary>
        /// <param name="ProductModel"></param>
        /// <returns></returns>
        [HttpDelete("remove/{id}")]
        public async Task<IActionResult> RemoveProduct(long id)
        {
            #region Validation

            if (id <= 0)
                return BadRequest(new { message = "Id must be greater than 0" });

            #endregion

            try
            {
                var product = await _unitOfWork.Product.Get(id);
                _unitOfWork.Product.Delete(product);

                return Ok(await _unitOfWork.Complete());
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }

        /// <summary>
        /// Fetch Product List by categoryId
        /// </summary>
        /// <returns>List of products by category id</returns>
        [HttpGet("list/{categoryId}")]
        public async Task<IActionResult> GetAllByCategoryId(long categoryId)
        {
            #region Validation

            if (categoryId <= 0)
                return BadRequest(new { message = "categoryId must be greater than 0" });

            #endregion

            try
            {
                var productList = await _unitOfWork.Product.GetAllByCategoryId(categoryId);
                await _unitOfWork.Complete();

                return Ok(productList);
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpPost("image/upload"), DisableRequestSizeLimit]
        public async Task<IActionResult> UploadProductImage()
        {
            try
            {
                var file = Request.Form.Files[0];
                if (file.Length > 0)
                {
                    long length = file.Length;

                    using var fileStream = file.OpenReadStream();
                    byte[] bytes = new byte[length];
                    await fileStream.ReadAsync(bytes, 0, (int)file.Length);

                    return Ok(new { data = Convert.ToBase64String(bytes) });
                }
                else
                {
                    return BadRequest(new { message = "No file" });
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = "API Error occured: " + ex.Message });
            }
        }

        [HttpPost("excel/upload"), DisableRequestSizeLimit]
        public async Task<IActionResult> UploadProductsExcel([FromQuery] int catId)
        {
            try
            {
                var file = Request.Form.Files[0];
                if (file.Length > 0)
                {
                    long length = file.Length;

                    using var fileStream = file.OpenReadStream();
                    byte[] bytes = new byte[length];
                    await fileStream.ReadAsync(bytes, 0, (int)file.Length);

                    //1. convert stream to excel
                    using (MemoryStream memStream = new MemoryStream(bytes))
                    {
                        ExcelPackage package = new ExcelPackage(memStream);

                        int lastRow = package.Workbook.Worksheets[0].Dimension.End.Row;

                        if (lastRow == 1)
                            return BadRequest(new { message = "Excel must contain product data!" });

                        ExcelWorksheet workSheet = package.Workbook.Worksheets[0];

                        List<Product> productList = new List<Product>();

                        //loop through rows of 1st worksheet to save products
                        for (int row = 2; row <= lastRow; row++)
                        {
                            try
                            {
                                Product product = new Product();

                                //code column
                                product.Code = workSheet.Cells[row, 1].Text;
                                //name column
                                product.Name = workSheet.Cells[row, 2].Text;
                                //description column
                                product.Description = workSheet.Cells[row, 3].Text;

                                //price column
                                decimal price = 0;
                                bool isPrice = Decimal.TryParse(workSheet.Cells[row, 4].Text, out price);
                                if (isPrice)
                                    product.Price = price;

                                product.CategoryId = catId;

                                //check if code does not exist, then add it
                                var p = await _unitOfWork.Product.GetByCode(product.Code);
                                await _unitOfWork.Complete();
                                if (p != null)
                                    productList.Add(product);
                            }
                            catch
                            {
                                //add product to error list
                            }

                            //add products tp transaction list for committing to DB
                            foreach (var p in productList)
                                await _unitOfWork.Product.Add(p);

                            //commit all new products to DB
                            if (productList.Count > 0)
                                await _unitOfWork.Complete();
                        }
                    }

                    return Ok(new { data = "success" });
                }
                else
                    return BadRequest(new { message = "No file" });
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = "API Error occured: " + ex.Message });
            }
        }

        //download products for a specific category
        [HttpGet("excel/download")]
        public async Task<IActionResult> DownloadProductsExcel([FromQuery] int catId)
        {
            try
            {
                var category = await _unitOfWork.Category.Get(catId);
                var products = await _unitOfWork.Product.GetAllByCategoryId(catId);
                await _unitOfWork.Complete();

                if (category == null)
                {
                    return Ok(new { message = "Category does not exist!" });
                }

                //map Products to ProductExportModel
                var productExportList = _mapper.Map<List<Product>, List<ProductExportModel>>(products.ToList());

                if (productExportList != null)
                {
                    using (ExcelPackage package = new ExcelPackage())
                    {
                        //create the excel file and set some properties
                        package.Workbook.Properties.Author = "Inventory System";
                        package.Workbook.Properties.Title = $"{category.Name} List";
                        package.Workbook.Properties.Created = DateTime.Now;

                        //create a new sheet
                        package.Workbook.Worksheets.Add(@"{category.Name}");
                        ExcelWorksheet ws = package.Workbook.Worksheets[0];
                        ws.Cells.Style.Font.Size = 11;
                        ws.Cells.Style.Font.Name = "Calibri";

                        //put the data in the sheet, starting from column A, row 1
                        ws.Cells["A1"].LoadFromCollection(productExportList, true);

                        //set some styling on the header row
                        var header = ws.Cells[1, 1, 1, ws.Dimension.End.Column];
                        header.Style.Font.Bold = true;
                        header.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        header.Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#eee"));

                        //loop the properties in list<t> to apply some data formatting based on data type and check for nested lists
                        var listObject = productExportList.First();
                        var columns_to_delete = new List<int>();
                        for (int i = 0; i < listObject.GetType().GetProperties().Count(); i++)
                        {
                            var prop = listObject.GetType().GetProperties()[i];
                            var range = ws.Cells[2, i + 1, ws.Dimension.End.Row, i + 1];

                            //check if the property is a List, if yes add it to columns_to_delete
                            if (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(List<>))
                                columns_to_delete.Add(i + 1);

                            //set the date format
                            if (prop.PropertyType == typeof(DateTime) || prop.PropertyType == typeof(DateTime?))
                                range.Style.Numberformat.Format = DateTimeFormatInfo.CurrentInfo.ShortDatePattern;

                            //set the decimal format
                            if (prop.PropertyType == typeof(decimal) || prop.PropertyType == typeof(decimal?))
                                range.Style.Numberformat.Format = "0.00";
                        }

                        //remove all lists from the sheet, starting with the last column
                        foreach (var item in columns_to_delete.OrderByDescending(x => x))
                            ws.DeleteColumn(item);

                        //auto fit the column width
                        ws.Cells[ws.Dimension.Address].AutoFitColumns();

                        //add some extra width to columns
                        for (int col = 1; col <= ws.Dimension.End.Column; col++)
                            ws.Column(col).Width += 3;

                        //send the excel back as byte array
                        byte [] excelByteArray = await package.GetAsByteArrayAsync();
                        return Ok(new { data = excelByteArray, fileName = $"{category.Name} List" });
                    }
                }
                else
                {
                    return Ok(new { message = "No products in category!" });
                }

            }
            catch(AppException ex)
            {
                return Ok(new { message = "Error exporting to Excel. " + ex.Message });
            }

        }
    }
}
