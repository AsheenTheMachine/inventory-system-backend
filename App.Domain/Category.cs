﻿
using App.Domain.Helpers;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace App.Domain
{
    public class Category : BaseEntity
    {
        public string Code { get; set; }
        
        //concurrency check to make sure 2 updates don't conflict
        [ConcurrencyCheck]
        public string Name { get; set; }
        
        public bool IsActive { get; set; } = false;

        public long UserId { get; set; }
    }
}
