﻿using System;
using System.Threading.Tasks;
using App.Repository.Interfaces;

namespace App.Repository.Helpers
{
    public interface IUnitOfWork 
    {

        IUserRepository User { get; }
        ICategoryRepository Category { get; }
        IProductRepository Product { get; }

        Task<int> Complete();
    }
}
