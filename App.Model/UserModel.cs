﻿using App.Model.Helpers;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace App.Model
{
    public class UserModel : BaseModel
    {
        [Required(ErrorMessage = "Name is required!")]
        [StringLength(50, ErrorMessage = "Name cannot be longer than 50 characters!")]
        public string Name { get; set; }
        
        [Required(ErrorMessage = "Surname is required!")]
        [StringLength(50, ErrorMessage = "Surname cannot be longer than 50 characters!")]
        public string Surname { get; set; }
        
        [Required(ErrorMessage = "Email is required!")]
        [StringLength(50, ErrorMessage = "Email cannot be longer than 100 characters!")]
        public string Email { get; set; }
        
        [Required]
        public string Password { get; set; }
    }
}
