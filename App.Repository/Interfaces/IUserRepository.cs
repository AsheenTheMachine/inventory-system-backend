﻿using System;
using System.Threading.Tasks;
using App.Domain;
using App.Repository.Helpers;

namespace App.Repository.Interfaces
{

    public interface IUserRepository : IGenericRepository<User>
    {
        Task<User> GetByEmail(string email);

        Task<User> GetByToken(Guid token);
    }
}
