using System.Threading.Tasks;

namespace App.Api.Services
{
    public interface IEmailService
    {
        Task Send(string to, string subject, string html, string from = null);
    }
}