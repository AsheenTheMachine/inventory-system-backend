﻿using App.Domain;
using App.Repository.Helpers;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace App.Repository.Interfaces
{

    public interface IProductRepository : IGenericRepository<Product>
    {
        Task<IEnumerable<Product>> GetAllByCategoryId(long categoryId);

        Task<Product> GetByCode(string code);
    }
}
