﻿using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using System.Security.Claims;

namespace App.Test.Helpers
{
    public class FakeUserFilter : IAsyncActionFlter
    {
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            context.HttpContext.User = new ClaimsPrincipal(new ClaimsIdentity(new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, "1"),
                new Claim(ClaimTypes.Name, "Asheen"),
                new Claim(ClaimTypes.Email, "asheenk@gmail.com")
            }));

            await next();
        }
    }
}
