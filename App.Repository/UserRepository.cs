﻿using System.Linq;
using App.Domain;
using App.Repository.Helpers;
using App.Domain.Helpers;
using App.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System;

namespace App.Repository
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        public UserRepository(DataContext context) : base(context) {}

        public async Task<User> GetByEmail(string email)
        {
            return await _context.User
                            .Where(e => e.Email == email)
                            .FirstOrDefaultAsync();
        }

        public async Task<User> GetByToken(Guid token)
        {
            return await _context.User
                            .Where(e => e.VerifyToken.Value == token)
                            .FirstOrDefaultAsync();
        }
    }
}
