# README #

.Net Core 5.0 Inventory System API

### What is this repository for? ###

* The is an inventory system backend API. To provide frontend apps with database interaction and processing.
* Version 1.0

### Technology Stack & Features ###
1.  ASP.Net Core 5.0
2.  C#
3.  Visual Studio Code
4.  Microsoft SQL Server
5.	Entity Framework Core
6.  Concurreny Check applied on EF fields
7.  Linq Lambda Expressions
8.  JWT Authentication
9.  Authorization
10.	xUnit Tests
11.	SOLID Principles (Dependancy Injection, Single Resposibility, Interface Segregation)
12.  Respository Pattern + Generic Repository
13.  Asynchronous Tasks
14. Unit of Work
15.	Swagger UI Documentation
16. Create categories
17. Create products
18. User registration with email verification


### How do I get set up? ###

#### The solution consists of the following projects ####

1.	App.API
2.	App.Domain
3.	App.Model
4.	App.Repository
5.	App.Test


#### Configuration ####

1.	Copy the clone link from this repo.
2.	Open Visual Studio Code or Visual Studio Professionl/Enterprise
3.	Click on clone repository and past the link. Alternatively, you may open Powershell and navigate to your projects folder. Then type `git clone CloneUrl` or right click, and your command pallete will automatically paste the link for you.

#### Dependencies

Visual Studio Code/Professional/Enterprise will automatically import dependecies required by the solution.

#### Database configuration

1.	Open Microsoft SQL Server
2.	Create a database called `inventory`
3.	Execute the inventory-system-database-script.sql script located in the `Database Scripts` folder, in your SQL Server database
4.	Update the connection string settings with your SQL Server database username & password. Connection strings are in the following files, `App.API/appSettings.json` and `App.Test/integrationsettings.json`

#### Email configuration
Update the Email SMTP settings found in `App.API/appSettings.json` with your SMTP settings. You may use GMail settings too. Make sure to navigate to this page https://www.google.com/settings/security/lesssecureapps & set to "Turn On"

### API Endpoints
Browse `http://localhost:4000/swagger` for API endpoints and relevant information

### App.Test project
Note:  To run the tests, you must comment out the `[Authorize]` attribute on the controllders in App.API. I've not applied the FakeUserFilter or catered for Authorization in my tests - Time is a factor :( 


### Who do I talk to? ###

* email asheenk@gmail.com for further assistance
