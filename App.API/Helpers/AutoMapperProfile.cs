﻿using AutoMapper;
using App.Domain;
using App.Model;

namespace App.UI.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            #region User
            CreateMap<User, RegisterModel>().ReverseMap();
            CreateMap<User, RegisterModel>();

            CreateMap<User, UserModel>().ReverseMap();
            CreateMap<User, UserModel>();
            #endregion

            #region Category
            CreateMap<Category, CategoryModel>().ReverseMap()
            .ForMember(x => x.Code, opt => opt.MapFrom(y => y.Code.Trim()));
            
            CreateMap<Category, CategoryModel>()
            .ForMember(x => x.Code, opt => opt.MapFrom(y => y.Code.Trim()));
            #endregion

            #region Product
            CreateMap<Product, ProductModel>().ReverseMap()
            .ForMember(x => x.Code, opt => opt.MapFrom(y => y.Code.Trim()));

            CreateMap<Product, ProductModel>()
            .ForMember(x => x.Code, opt => opt.MapFrom(y => y.Code.Trim()));

            CreateMap<Product, ProductExportModel>().ReverseMap();
            CreateMap<Product, ProductExportModel>();
            #endregion
        }
    }
}
