﻿
using App.Domain.Helpers;
using System.Collections.Generic;
using System;
using System.ComponentModel.DataAnnotations;

namespace App.Domain
{
    public class User : BaseEntity
    {
        //concurrency check to make sure 2 updates don't conflict
        [ConcurrencyCheck]
        public string Name { get; set; }
        
        [ConcurrencyCheck]
        public string Surname { get; set; }
        
        [ConcurrencyCheck]
        public string Email { get; set; }
        
        public byte[] PasswordHash { get; set; }
        
        public byte[] PasswordSalt { get; set; }

        public bool IsActive { get; set; } = false;

        public Guid? VerifyToken { get; set; } = Guid.NewGuid();

    }
}
