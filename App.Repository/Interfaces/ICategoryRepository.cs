﻿using App.Domain;
using App.Repository.Helpers;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace App.Repository.Interfaces
{

    public interface ICategoryRepository : IGenericRepository<Category>
    {
         Task<List<Category>> GetAllByUserId(long userId);
         Task<Category> GetByCode(string code);
    }
}
