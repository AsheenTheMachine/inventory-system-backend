﻿using System;

namespace App.Model.Helpers
{
    public class BaseModel : IBaseModel
    {
        public long Id { get; set; }

        public long GetId()
        {
            return Id;
        }

    }
}
