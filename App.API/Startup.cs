using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using App.Domain.Helpers;
using App.Repository.Interfaces;
using App.Repository.Helpers;
using App.Repository;
using AutoMapper;
using System;
using App.Api.Helpers;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Http.Features;
using App.Api.Services;
using Microsoft.OpenApi.Models;

namespace App
{
    public class Startup
    {
        private IConfiguration _configuration { get; }
        
        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddDbContext<DataContext>();
            services.AddDbContext<DataContext>(ServiceLifetime.Scoped);

            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

              //swagger documentation of all end points
            services.AddSwaggerGen(c => {  
                    c.SwaggerDoc("v1", new OpenApiInfo {  
                        Version = "v1",  
                            Title = "Inventory System API",  
                            Description = "Swagger api information for inventory system backend",  
                            TermsOfService = new Uri("http://localhost:4000/api/terms"),
                            Contact = new OpenApiContact {  
                                Name = "Asheen Kamlal",
                                Email = "asheenk@gmail.com",  
                                Url = new Uri("https://www.linkedin.com/in/asheen-singh/"),
                            },  
                            License = new OpenApiLicense {  
                                Name = "Use under OpenApiLicense",  
                                Url = new Uri("http://localhost:4000/api/license"),
                            }  
                    });  
                });

            // configure strongly typed settings objects
            var appSettingsSection = _configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);

            // configure jwt authentication
            var appSettings = appSettingsSection.Get<AppSettings>();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);

            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.Events = new JwtBearerEvents
                {
                    OnTokenValidated = context =>
                    {
                        var userId = int.Parse(context.Principal.Identity.Name);
                        if (userId == 0)
                        {
                            // return unauthorized if user no longer exists
                            context.Fail("You are not authorized to use this service!");
                        }
                        
                        return Task.CompletedTask;
                    }
                };
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });

            //not using the AllowAnyOrigin() as the combination of AllowAnyOrigin and AllowCredentials 
            //is considered an insecure CORS configuration
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy", builder => builder
                .WithOrigins("http://localhost:4200")
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials());
            });

            //file upload config
            services.Configure<FormOptions>(o => 
            { 
                o.ValueLengthLimit = int.MaxValue; 
                o.MultipartBodyLengthLimit = int.MaxValue; 
                o.MemoryBufferThreshold = int.MaxValue; 
            });

            services.AddControllers()
            .AddNewtonsoftJson(options =>
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            );

            //Dependency Injection
            services.AddScoped<IEmailService, EmailService>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<ICategoryRepository, CategoryRepository>();
            services.AddScoped<IProductRepository, ProductRepository>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app)
        {
            app.UseRouting();

            // global cors policy
            app.UseCors("CorsPolicy");

            // generated swagger json and swagger ui middleware
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Inventory System API V1");
            });

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
