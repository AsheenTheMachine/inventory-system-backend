﻿using System.Linq;
using App.Domain;
using App.Repository.Helpers;
using App.Domain.Helpers;
using App.Repository.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace App.Repository
{
    public class ProductRepository : GenericRepository<Product>, IProductRepository
    {
        public ProductRepository(DataContext context) : base(context) {}

        public async Task<IEnumerable<Product>> GetAllByCategoryId(long categoryId)
        {
            return await _context.Product
                            .Where(c => c.CategoryId == categoryId)
                            .OrderBy(x => x.Name)
                            .ToListAsync();
        }

        public async Task<Product> GetByCode(string code)
        {
            return await _context.Product
                            .Where(c => c.Code.CompareTo(code) == 1)
                            .FirstOrDefaultAsync();
        }
       
    }
}
